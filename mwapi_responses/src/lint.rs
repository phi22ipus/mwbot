/*
 * SPDX-License-Identifier: GPL-3.0-or-later
 * Copyright (C) 2023 Kunal Mehta <legoktm@debian.org>
 */

use serde::Deserialize;

#[derive(Clone, Debug, Deserialize)]
pub struct TemplateInfo {
    pub name: Option<String>,
}

#[derive(Clone, Debug, Deserialize)]
pub struct TemplateParam {
    pub name: Option<String>,
}

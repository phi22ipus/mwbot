/*
Copyright (C) 2021 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use mwtimestamp::Timestamp;
use serde::de::IntoDeserializer;
use serde::{de, Deserialize};

/// Not public, for internal use only.
///
/// Wrapper for notificationtimestamp which is Option<Timestamp>
/// except uses empty string for the none case
#[doc(hidden)]
pub fn deserialize_notificationtimestamp<'de, D>(
    deserializer: D,
) -> Result<Option<Timestamp>, D::Error>
where
    D: de::Deserializer<'de>,
{
    let opt = Option::<String>::deserialize(deserializer)?;
    let opt = opt.as_deref();
    match opt {
        None | Some("") => Ok(None),
        Some(s) => Timestamp::deserialize(s.into_deserializer()).map(Some),
    }
}

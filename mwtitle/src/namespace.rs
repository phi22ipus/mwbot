/*
Copyright (C) 2023 Ben Kurtovic <ben@benkurtovic.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//! These are [namespace constants](https://www.mediawiki.org/wiki/Manual:Namespace_constants)
//! for built-in MediaWiki namespaces.

pub const NS_MAIN: i32 = 0;
pub const NS_TALK: i32 = 1;
pub const NS_USER: i32 = 2;
pub const NS_USER_TALK: i32 = 3;
pub const NS_PROJECT: i32 = 4;
pub const NS_PROJECT_TALK: i32 = 5;
pub const NS_FILE: i32 = 6;
pub const NS_FILE_TALK: i32 = 7;
pub const NS_MEDIAWIKI: i32 = 8;
pub const NS_MEDIAWIKI_TALK: i32 = 9;
pub const NS_TEMPLATE: i32 = 10;
pub const NS_TEMPLATE_TALK: i32 = 11;
pub const NS_HELP: i32 = 12;
pub const NS_HELP_TALK: i32 = 13;
pub const NS_CATEGORY: i32 = 14;
pub const NS_CATEGORY_TALK: i32 = 15;
pub const NS_SPECIAL: i32 = -1;
pub const NS_MEDIA: i32 = -2;

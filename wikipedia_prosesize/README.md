# wikipedia_prosesize

[![crates.io](https://img.shields.io/crates/v/wikipedia_prosesize.svg)](https://crates.io/crates/wikipedia_prosesize)
[![docs.rs](https://img.shields.io/docsrs/wikipedia_prosesize?label=docs.rs)](https://docs.rs/wikipedia_prosesize)
[![docs (main)](https://img.shields.io/badge/doc.wikimedia.org-green?label=docs%40main)](https://doc.wikimedia.org/mwbot-rs/mwbot/wikipedia_prosesize/)
[![pipeline status](https://gitlab.wikimedia.org/repos/mwbot-rs/mwbot/badges/main/pipeline.svg)](https://gitlab.wikimedia.org/repos/mwbot-rs/mwbot/-/commits/main)
[![coverage report](https://img.shields.io/endpoint?url=https%3A%2F%2Fdoc.wikimedia.org%2Fcover%2Fmwbot-rs%2Fmwbot%2Fcoverage%2Fcoverage.json)](https://doc.wikimedia.org/cover/mwbot-rs/mwbot/coverage)

Calculate Wikipedia prose size

This crate is a rough port of the [Wikipedia Prosesize script](https://en.wikipedia.org/wiki/Wikipedia:Prosesize)
that allows for counting the bytes of prose on a page rather than the wikitext
markup or generated HTML.

You will most likely fetch `ImmutableWikicode` using the [parsoid](https://docs.rs/parsoid)
crate.

The response from `prosesize()` provides the text-only prose size, word count and text-only
references size. Enabling the optional `serde-1` feature makes the size struct serializable
and deserializable.

### Contributing
`wikipedia_prosesize` is part of the [`mwbot-rs` project](https://www.mediawiki.org/wiki/Mwbot-rs).
We're always looking for new contributors, please [reach out](https://www.mediawiki.org/wiki/Mwbot-rs#Contributing)
if you're interested!

## License
This crate is released under GPL-3.0-or-later.
See [COPYING](./COPYING) for details.

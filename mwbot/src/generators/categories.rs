// SPDX-FileCopyrightText: 2023 xtex <xtexchooser@duck.com>
// SPDX-License-Identifier: GPL-3.0-or-later

use super::{Generator, ParamValue, SortDirection};

/// Get all categories that included in the pages.
///
/// See [API documentation](https://www.mediawiki.org/wiki/API:Categories) for more details.
#[derive(Generator)]
#[params(generator = "categories", gcllimit = "max")]
pub struct Categories {
    #[param("titles")]
    titles: Vec<String>,
    #[param("gclcategories")]
    categories: Option<Vec<String>>,
    #[param("gclshow")]
    filter: Option<Filter>,
    #[param("gcldir")]
    sort: Option<SortDirection>,
}

pub enum Filter {
    Hidden,
    NonHidden,
}

impl ParamValue for Filter {
    fn stringify(&self) -> String {
        match self {
            Self::Hidden => "hidden",
            Self::NonHidden => "!hidden",
        }
        .to_string()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::tests::testwp;

    #[tokio::test]
    async fn test_categories() {
        let bot = testwp().await;
        let gen = Categories::new(vec!["Mwbot-rs/Categorized".to_string()]);
        dbg!(gen.params());
        let mut pages = gen.generate(&bot);

        let mut found = Vec::new();

        while let Some(page) = pages.recv().await {
            let page = page.unwrap();
            dbg!(page.title());

            found.push(page.title().to_string());
        }

        assert!(found.contains(&"Category:Mwbot-rs".to_string()));
    }
}

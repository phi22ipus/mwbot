// SPDX-FileCopyrightText: 2023 Misato Kano <me@mirror-kt.dev>
// SPDX-License-Identifier: GPL-3.0-or-later

use super::Generator;

/// Get all unconnected pages to Wikibase.
#[derive(Generator)]
#[params(
    generator = "querypage",
    gqppage = "UnconnectedPages",
    gqplimit = "max"
)]
pub struct UnconnectedPages {
    #[param("gqpoffset")]
    offset: Option<u64>,
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::tests::testwp;

    #[tokio::test]
    async fn test_unconnected_pages() {
        let bot = testwp().await;
        let gen = UnconnectedPages::new();
        dbg!(gen.params());

        let mut pages = gen.generate(&bot);
        let mut count = 0;

        while let Some(page) = pages.recv().await {
            let page = page.unwrap();
            dbg!(page.title());

            if count >= 5 {
                break;
            }
            count += 1;
        }

        assert_eq!(count, 5);
    }
}

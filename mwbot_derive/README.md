# mwbot_derive

[![crates.io](https://img.shields.io/crates/v/mwbot_derive.svg)](https://crates.io/crates/mwbot_derive)
[![docs.rs](https://img.shields.io/docsrs/mwbot_derive?label=docs.rs)](https://docs.rs/mwbot_derive)
[![docs (main)](https://img.shields.io/badge/doc.wikimedia.org-green?label=docs%40main)](https://doc.wikimedia.org/mwbot-rs/mwbot/mwbot_derive/)
[![pipeline status](https://gitlab.wikimedia.org/repos/mwbot-rs/mwbot/badges/main/pipeline.svg)](https://gitlab.wikimedia.org/repos/mwbot-rs/mwbot/-/commits/main)
[![coverage report](https://img.shields.io/endpoint?url=https%3A%2F%2Fdoc.wikimedia.org%2Fcover%2Fmwbot-rs%2Fmwbot%2Fcoverage%2Fcoverage.json)](https://doc.wikimedia.org/cover/mwbot-rs/mwbot/coverage)

This crate provides the proc_macro for [`mwbot`](https://docs.rs/mwbot/),
please refer to its documentation for usage.

## License
This crate is released under GPL-3.0-or-later.
See [COPYING](./COPYING) for details.
